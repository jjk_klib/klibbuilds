FROM gcc:8 as klibbuilds_base

MAINTAINER Jason Kushmaul <jasonkushmaul@gmail.com>
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && \
      apt-get -y install git wget cmake g++ gcc libgtest-dev uncrustify gcovr lcov doxygen valgrind cppcheck protobuf-c-compiler protobuf-compiler libprotobuf-c-dev libprotobuf-dev libprotobuf-c1  \
                         python python-pip && \
      ldconfig
RUN   pip install --upgrade python-gitlab

FROM klibbuilds_base AS klibbuilds_googletest
RUN   cd /usr/src/googletest && \
      rm -rf build && mkdir build && cd build && \
      cmake -DCMAKE_INSTALL_PREFIX:PATH=/usr .. && make

FROM klibbuilds_googletest
RUN cd /usr/src/googletest/build && make install
RUN ls /usr/include/gmock/
RUN ls /usr/include/gtest
