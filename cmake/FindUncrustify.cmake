IF(NOT DEFINED UNCRUSTIFY_FILES)
    SET(UNCRUSTIFY_FILES  ${C_SOURCES} ${C_HEADERS} ${TEST_SOURCES})
ENDIF(NOT DEFINED UNCRUSTIFY_FILES)


find_program(UNCRUSTIFY uncrustify)
add_custom_target(tidy
                    WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}
                    COMMENT "tidy..."
                    COMMAND uncrustify -q -c .uncrustify ${UNCRUSTIFY_FILES}  --no-backup
                )

add_custom_target(checkstyle
                    WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}
                    COMMENT "checkstyle..."
                    COMMAND bash klibbuilds/cmake/uncrustify.sh ${UNCRUSTIFY_FILES}
                )
 
