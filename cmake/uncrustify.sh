#!/bin/bash

shift

OUTPUT=`uncrustify -c .uncrustify --check $@ | grep -i fail`
COUNT=`echo -n "$OUTPUT" | wc -l`
if [ "$COUNT"  != "0" ]; then
  echo "Checkstyle failed: $COUNT files" > /dev/stderr
  echo "$OUTPUT" > /dev/stderr
  exit -1
fi
