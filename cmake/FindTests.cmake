
find_package(GTest REQUIRED)

# Add test cpp file
add_executable(testexecutable ${TEST_SOURCES})
set_target_properties(testexecutable PROPERTIES COMPILE_FLAGS "-fPIC -g -std=c++11 -Wall --coverage" PROPERTIES LINK_FLAGS="-g --gcov")

add_dependencies(testexecutable coverage)

# Link test executable against gtest & gtest_main
target_link_libraries(testexecutable gtest pthread coverage gcov ${EXTRA_LIBRARIES})
add_test(
    NAME testexecutable
    COMMAND ./testexecutable
)
