find_program(VALGRIND valgrind)
add_custom_target(memcheck
                  COMMENT "Memcheck..."
                  COMMAND valgrind --log-fd=1 --error-exitcode=1 --trace-children=yes --leak-check=full --show-leak-kinds=all --track-origins=yes ./testexecutable
                )
add_dependencies(memcheck testexecutable)
