set(CMAKE_EXPORT_COMPILE_COMMANDS ON)
set(BUILD_SHARED_LIBS OFF)
FILE(GLOB_RECURSE C_SOURCES ${CMAKE_SOURCE_DIR}/src/*.c)
FILE(GLOB_RECURSE C_HEADERS ${CMAKE_SOURCE_DIR}/include/*.h)
FILE(GLOB_RECURSE TEST_SOURCES ${CMAKE_SOURCE_DIR}/tests/*.cpp)


function(__setup_klib_project projectname)
    include_directories(${CMAKE_SOURCE_DIR}/include )
	
    if(ARGV1)
        list(APPEND KLIB_DEPS ${ARGV1})
    endif()
    
    list(APPEND THESE_LIBS ${KLIB_DEPS})
    message("KLIB_DEPS=${KLIB_DEPS}")
    foreach(DEP ${KLIB_DEPS})
        list(APPEND THESE_DEBUG_LIBS ${DEP}-debug)
    endforeach()
    
    list(APPEND THESE_LIBS ${EXTRA_LIBRARIES})
    list(APPEND THESE_DEBUG_LIBS ${EXTRA_LIBRARIES})
    
    message("THESE_DEBUG_LIBS=${THESE_DEBUG_LIBS}")	
    set_target_properties(${projectname}  PROPERTIES COMPILE_FLAGS "-fPIC -Wall")
    target_link_libraries(${projectname} ${THESE_LIBS})

    #won't have projectname, ktypes, but will have ci project name klibktypes in gitlab-ci
    set_target_properties(${projectname}-debug PROPERTIES COMPILE_FLAGS "-fPIC -g -Wall")
    set_target_properties(${projectname}-debug PROPERTIES LINK_FLAGS "-g")
    target_link_libraries(${projectname}-debug ${THESE_DEBUG_LIBS})
    add_custom_target(klib${projectname}-debug DEPENDS ${projectname}-debug)

    project(coverage)
    add_library(coverage ${C_SOURCES})
    set_target_properties(coverage PROPERTIES COMPILE_FLAGS "-fPIC -g -Wall --coverage")
    set_target_properties(coverage PROPERTIES LINK_FLAGS "-g")
    target_link_libraries(coverage ${THESE_DEBUG_LIBS})

    find_package(Cppcheck)
    find_package(Uncrustify)
    find_package(DoxygenHelper)


    #Tests
    project(testexecutable)
    find_package(Tests)
    find_package(Valgrind)
    find_package(CoverageReport)
    
    target_link_libraries(testexecutable ${THESE_DEBUG_LIBS})    
    add_dependencies(coverage ${projectname} analysis checkstyle)
    add_custom_target(devbuild DEPENDS tidy memcheck klib${projectname}-debug)
    
endfunction(__setup_klib_project)

function(setup_klib_executable projectname)
    project(${projectname})
    add_executable(${projectname} ${C_SOURCES})
    project(${projectname}-debug)
    add_executable(${projectname}-debug ${C_SOURCES})
    
	__setup_klib_project(${projectname} ${ARGV2})

endfunction(setup_klib_executable)

function(setup_klib_library projectname)
    project(${projectname})
    add_library(${projectname} ${C_SOURCES})
    project(${projectname}-debug)
    add_library(${projectname}-debug ${C_SOURCES})
    
	__setup_klib_project(${projectname} ${ARGV2})
endfunction(setup_klib_library)

function(FATAL_EXECUTE)
	set(options "")
    set(oneValueArgs  WORKING_DIRECTORY)
    set(multiValueArgs COMMAND)
	cmake_parse_arguments(FATAL_EXECUTE "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})
	execute_process ( COMMAND ${FATAL_EXECUTE_COMMAND} WORKING_DIRECTORY ${FATAL_EXECUTE_WORKING_DIRECTORY} RESULT_VARIABLE retcode)
	if ( NOT ${retcode} STREQUAL "0")
		message(FATAL_ERROR "bad return (${retcode}) from command: ${FATAL_EXECUTE_COMMAND} from within ${FATAL_EXECUTE_WORKING_DIRECTORY}")
    endif()
endfunction(FATAL_EXECUTE)

function(use_klib_project klib_projectname version maketarget)
        set(URL "https://jkushmaul.gitlab.io/${klib_projectname}/${klib_projectname}-${version}.tar.gz")
        set(DLPATH "${CMAKE_BINARY_DIR}/${klib_projectname}-${version}.tar.gz")
        set(EXT_DIR "${CMAKE_BINARY_DIR}/${klib_projectname}-${version}")
        if (NOT EXISTS "${DLPATH}")
            file(DOWNLOAD "${URL}" "${DLPATH}")
        endif()
        if (NOT EXISTS "${EXT_DIR}")
                execute_process( COMMAND tar -xf "${DLPATH}" )
        endif()
        
        add_custom_target(${klib_projectname}-import
                    WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
                    COMMAND bash -c "rm -rf ${klib_projectname}-${version}/*"
                    COMMAND mkdir -p ${klib_projectname}-${version}/lib
                    COMMAND cp -r ../../${klib_projectname}/include ${klib_projectname}-${version}/
                    COMMAND bash -c "cp -f ../../${klib_projectname}/build/${lib_name}*.a ${klib_projectname}-${version}/lib/"
                )

        include_directories(${EXT_DIR}/include)
        link_directories(${EXT_DIR}/lib)
endfunction(use_klib_project)
